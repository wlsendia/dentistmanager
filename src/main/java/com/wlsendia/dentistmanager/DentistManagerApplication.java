package com.wlsendia.dentistmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DentistManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DentistManagerApplication.class, args);
    }

}
