package com.wlsendia.dentistmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class WhatsNameRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

}
