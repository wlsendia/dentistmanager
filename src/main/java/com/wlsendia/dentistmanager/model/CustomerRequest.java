package com.wlsendia.dentistmanager.model;

import com.wlsendia.dentistmanager.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {

    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 2, max = 20)
    private String customerPhone;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

}
