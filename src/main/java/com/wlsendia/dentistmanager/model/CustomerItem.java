package com.wlsendia.dentistmanager.model;

import com.wlsendia.dentistmanager.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {
    private String customerName;

    private String customerPhone;

    private String gender;//출력용이니까 스트링

    private LocalDateTime reservationDate;

    private String reservationDetails;

    private String dormantDay;//
}
