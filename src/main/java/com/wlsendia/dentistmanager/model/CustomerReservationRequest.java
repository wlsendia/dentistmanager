package com.wlsendia.dentistmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerReservationRequest {

    @NotNull
    private LocalDateTime reservationDate;

    @NotNull
    @Length(min = 2, max = 90)
    private String reservationDetails;
}
