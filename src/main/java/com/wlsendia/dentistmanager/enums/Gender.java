package com.wlsendia.dentistmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Gender {

    MAN("남자", "주민번호 뒷자리 1 or 3"),

    WOMAN("여자", "주민번호 뒷자리 2 or 4"),

    WHAT("모름", "예전에 가입해서 기입 안 된 손님들");

    private final String name;

    private final String description;
}
