package com.wlsendia.dentistmanager.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DormantDay {

    READY("대기","30일 이하"),

    NOT_READY("방문요망", "31~90일"),

    DORMANT_CUSTOMER("휴면", "90일 초과");

    private final String name;

    private final String description;
}