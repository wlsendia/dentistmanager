package com.wlsendia.dentistmanager.repository;

import com.wlsendia.dentistmanager.entity.DentistCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<DentistCustomer, Long> {
    List<DentistCustomer> findAllByCustomerName(String customerName);
}
