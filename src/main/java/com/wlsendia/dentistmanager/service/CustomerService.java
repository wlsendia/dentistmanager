package com.wlsendia.dentistmanager.service;

import com.wlsendia.dentistmanager.entity.DentistCustomer;
import com.wlsendia.dentistmanager.enums.DormantDay;
import com.wlsendia.dentistmanager.model.CustomerItem;
import com.wlsendia.dentistmanager.model.CustomerRequest;
import com.wlsendia.dentistmanager.model.CustomerReservationRequest;
import com.wlsendia.dentistmanager.model.WhatsNameRequest;
import com.wlsendia.dentistmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

import static com.wlsendia.dentistmanager.enums.Gender.WHAT;


@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository repository;

    private List<DentistCustomer> getDentistCustomers() {
        return repository.findAll();
    }

    private DentistCustomer getDentistCustomer(long id) {
        return repository.findById(id).orElseThrow();
    }

    private DentistCustomer save(DentistCustomer customer) {
        return repository.save(customer);
    }

    public void setCustomer(CustomerRequest request) {
        DentistCustomer addData = new DentistCustomer();

        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setGender(request.getGender());
        addData.setJoinDate(LocalDate.now());
        addData.setRecentDate(LocalDate.now());
        // 예약정보도 넣을 수 있게(선택)

        save(addData);


    }

    public void putReservation(CustomerReservationRequest request, long id) {
        DentistCustomer temp = getDentistCustomer(id);

        temp.setReservationDate(request.getReservationDate());
        temp.setReservationDetails(request.getReservationDetails());
        temp.setRecentDate(LocalDate.now());
        //"2023-01-20T14:30" GMT 이런 거 신경쓸 필요x

        save(temp);

    }

    public List<CustomerItem> getCustomers() {//전체출력 이름 전화번호 예약날짜 예약내용
        List<CustomerItem> result = new LinkedList<>();
        List<DentistCustomer> origin = getDentistCustomers();

        for (DentistCustomer list : origin) {
            CustomerItem temp = new CustomerItem();
            temp.setCustomerName(list.getCustomerName());
            temp.setCustomerPhone(list.getCustomerPhone());
            temp.setGender(list.getGender().getName());
            temp.setReservationDate(list.getReservationDate());
            temp.setReservationDetails(list.getReservationDetails());

            long tempDay = ChronoUnit.DAYS.between(list.getRecentDate(), LocalDate.now());
            DormantDay DormantTemp;
            if (tempDay <= 30) {
                DormantTemp = DormantDay.READY;
            } else if (tempDay <= 90) {
                DormantTemp = DormantDay.NOT_READY;
            } else {
                DormantTemp = DormantDay.DORMANT_CUSTOMER;
            }
            temp.setDormantDay(DormantTemp.getName());

            result.add(temp);
        }

        return result;

    }

    public List<CustomerItem> getCustomer(WhatsNameRequest request) {//동명이인 전부 출력
        List<CustomerItem> result = new LinkedList<>();
        List<DentistCustomer> origin = repository.findAllByCustomerName(request.getCustomerName());//Repository에서 가져옴

        for (DentistCustomer list : origin) {
            CustomerItem temp = new CustomerItem();
            temp.setCustomerName(list.getCustomerName());
            temp.setCustomerPhone(list.getCustomerPhone());
            temp.setGender(list.getGender().getName());
            temp.setReservationDate(list.getReservationDate());
            temp.setReservationDetails(list.getReservationDetails());

            long tempDay = ChronoUnit.DAYS.between(list.getRecentDate(), LocalDate.now());
            DormantDay DormantTemp;
            if (tempDay <= 30) {
                DormantTemp = DormantDay.READY;
            } else if (tempDay <= 90) {
                DormantTemp = DormantDay.NOT_READY;
            } else {
                DormantTemp = DormantDay.DORMANT_CUSTOMER;
            }
            temp.setDormantDay(DormantTemp.getName());

            result.add(temp);
        }

        return result;

    }

    public void putCustomerInfo(CustomerRequest request, long id) {
        DentistCustomer origin2Put = getDentistCustomer(id);

        origin2Put.setCustomerName(request.getCustomerName());
        origin2Put.setCustomerPhone(request.getCustomerPhone());
        origin2Put.setGender(request.getGender());
        origin2Put.setRecentDate(LocalDate.now());

        save(origin2Put);
    }

    public void unknownGender() {//기존 미입력자들 한번에 미입력으로 변경해주는 코드
        List<DentistCustomer> all = getDentistCustomers();

        for (DentistCustomer list : all) {
            if (list.getGender() == null) {
                list.setGender(WHAT);
                save(list);
            }
        }

    }
}
