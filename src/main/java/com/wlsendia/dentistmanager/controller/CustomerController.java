package com.wlsendia.dentistmanager.controller;

import com.wlsendia.dentistmanager.model.CustomerItem;
import com.wlsendia.dentistmanager.model.CustomerRequest;
import com.wlsendia.dentistmanager.model.CustomerReservationRequest;
import com.wlsendia.dentistmanager.model.WhatsNameRequest;
import com.wlsendia.dentistmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    //매핑 이름 바꾸기
    private final CustomerService service;

    @PostMapping("/setcustomer")
    public String setCustomer(@RequestBody @Valid CustomerRequest request){
        service.setCustomer(request);

        return "1";

    }
    @PutMapping("/putreservation/id/{id}")
    public String putReservation(@RequestBody @Valid CustomerReservationRequest request, @PathVariable long id){
        //"2023-01-20T14:30" GMT 이런 거 신경쓸 필요x
        service.putReservation(request, id);

        return "2";
    }

    @GetMapping("/getcustomers")
    public List<CustomerItem> getCustomers(){
        return service.getCustomers();
    }

    @PostMapping("/whatsname")
    public List<CustomerItem> getCustomer(@RequestBody @Valid WhatsNameRequest request){ //
        List<CustomerItem> result = new LinkedList<>();
        result = service.getCustomer(request);

        return result;
    }

    @PutMapping("/putcustomerinfo/id/{id}")
    public String putCustomerInfo(@RequestBody @Valid CustomerRequest request, @PathVariable long id){
        service.putCustomerInfo(request, id);

        return "5";

    }

    @GetMapping("/un")
    public String unknownGender(){
        service.unknownGender();
        return "WHATOK";
    }
}